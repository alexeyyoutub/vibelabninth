import java.io.IOException;
import java.util.Scanner;

public class Ninth {

    public static void main(String[] args) {

        String text = new String();
        Scanner scanner = new Scanner(System.in);
        text = scanner.nextLine();
        String[] words = text.split(" ");
        int c;
        try{
            for (String word: words){
                c = (int) word.charAt(word.length()-1);
                if (c > 32 && c < 48)
                    word.replace(word.substring(word.length()-1), "");
                if (word.length() > 10)
                    throw new IOException("Длина слова \"" + word + "\" больше 10 символов!");
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}